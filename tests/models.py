# Django
from django.contrib.auth.models import User
from django.db import models

# local Django
from items.models import Category, Lesson, Item
from users.models import Student


# Create your models here.


class TestMode(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return '{} {}'.format(self.pk, self.name)


class Test(models.Model):
    category = models.ForeignKey(Category)
    date = models.DateField(auto_now_add=True)
    lesson = models.ForeignKey(Lesson)
    mode = models.ForeignKey(TestMode)
    score = models.IntegerField(null=True)
    type = models.IntegerField()
    user = models.ForeignKey(User)

    def __str__(self):
        return '{}'.format(self.pk)


class Question(models.Model):
    answer = models.CharField(max_length=30, null=True)
    item = models.ForeignKey(Item)
    result = models.IntegerField()
    test = models.ForeignKey(Test)

    def __str__(self):
        return '{} - {}'.format(self.pk, self.item)


class Session(models.Model):
    index = models.IntegerField(default=0)
    last_session = models.DateTimeField(blank=True, null=True)  # if date test finished else no test yet
    lesson = models.ForeignKey(Lesson)
    user = models.ForeignKey(User)
