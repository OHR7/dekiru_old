# Standard library
from datetime import datetime, timezone, timedelta
from random import shuffle, randint

# Django
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import DetailView, ListView, TemplateView

# Local Django
from dekiru.decorators import check_test
from items.models import Item, ItemTracking, Lesson, Option
from tests.models import Question, Session, Test, TestMode
from users.models import Status


# Create your views here.


@method_decorator(check_test, name='dispatch')
class TestHistory(ListView):
    model = Test
    template_name = "tests/test-history.html"

    def get_queryset(self):
        lesson = get_object_or_404(Lesson, pk=self.kwargs['lesson'])
        return Test.objects.filter(lesson=lesson.id, user=self.request.user)


@method_decorator(check_test, name='dispatch')
class TestResult(DetailView):
    model = Test
    template_name = "tests/test-result.html"

    def get_context_data(self, **kwargs):
        context = super(TestResult, self).get_context_data(**kwargs)
        # Get the pk of the test from kwargs
        test_pk = self.kwargs.get(self.pk_url_kwarg)
        context['questions'] = Question.objects.filter(test=test_pk)
        return context


@method_decorator(check_test, name='dispatch')
class TestMenu(TemplateView):

    template_name = "tests/test-menu.html"

    def get_context_data(self, **kwargs):
        context = super(TestMenu, self).get_context_data(**kwargs)
        # Passes the lesson object to the template in order to pass it to next view
        context['lesson'] = self.kwargs['lesson']
        test_status, test_date = self.get_test_status(self.kwargs['lesson'])
        context['test_status'] = test_status
        context['test_date'] = test_date
        # print(context['test_status'])

        return context

    def get_test_status(self, lesson_id):
        try:
            session = Session.objects.get(lesson=lesson_id, user=self.request.user)
            time = datetime.now(timezone.utc) - session.last_session
            time, remainder = divmod(time.seconds, 3600)  # TODO leave Remainder for later use of countdown
            test_date = session.last_session + timedelta(days=1)
            # print(time)
            # print(remainder)
            if time >= 24:
                return True, test_date
            else:
                return False, test_date
        except Session.DoesNotExist:
            print(Session.DoesNotExist)
            return True
        except Exception as e:
            print(e)
            return True


@check_test
def create_test(request, lesson_id, mode_id, limit=20):
    # Create the questions array
    try:
        session = Session.objects.get(user=request.user, lesson_id=lesson_id)
    except Session.DoesNotExist:
        session = Session(user=request.user, lesson_id=lesson_id)
        session.save()

    query = ItemTracking.objects.filter(
        lesson__id=lesson_id,
        user=request.user,
        next=session.index
    ).values_list('item__id', flat=True)

    lesson = Lesson.objects.get(id=lesson_id)
    mode = TestMode.objects.get(id=mode_id)
    status_obj = Status.objects.get(user=request.user)
    items = [i for i in query]

    if len(items) < limit:
        remaining_items = 20 - len(items)
        missed_items_query = ItemTracking.objects.filter(
                            lesson__id=lesson_id,
                            user=request.user,
                            box=-1
                        ).values_list('item__id', flat=True)[:remaining_items]
        missed_items = [i for i in missed_items_query]
        print('Missed Items: ')
        print(missed_items)
        print("\n")
        items += missed_items

    if len(items) < limit:
        # print(len(items))
        untracked_items = []
        items_query = Item.objects.filter(lesson=lesson_id)
        for i in items_query:
            if len(items) >= limit:
                break
            if not i.itemtracking.filter(lesson=lesson_id).exists():
                untracked_items.append(i.id)
        print('Untracked Items: ')
        print(untracked_items)
        print("\n")
        items += untracked_items

    shuffle(items)
    print('Items List: ')
    print(items)
    print("\n")

    try:
        first_index = items[0]
    except IndexError:
        if session.index > 8:
            session.index = 0
        else:
            session.index += 1
        session.save()
        # TODO Add code to update session date
        return render(request, 'tests/no-items.html')

    # Create a new Test Object
    test = Test(lesson=lesson, category=lesson.category, mode=mode, user=request.user, type=0)
    test.save()
    # print(mode)

    # Create a new Test
    status_obj.items = items
    # Variable to check if the user has a incomplete test
    status_obj.in_test = True
    status_obj.current_test = test.id
    # Set the index to the first item in this test
    status_obj.question_index = items[0]
    status_obj.save()

    # return redirect('question-test', first_index, test.id)
    return redirect('question', first_index, test.id, 0)


@check_test
def create_review(request, lesson_id, mode_id, test_filter):
    # Create the questions array
    if test_filter == '0':
        query = Item.objects.filter(lesson=lesson_id).values_list('id', flat=True)
    elif test_filter == '1':
        query = ItemTracking.objects.filter(
            item__lesson=lesson_id,
            user=request.user
        ).values_list('item__id', flat=True)
    else:
        query = Item.objects.filter(itemtracking__isnull=True, lesson=lesson_id).values_list('id', flat=True)

    lesson = Lesson.objects.get(id=lesson_id)
    mode = TestMode.objects.get(id=mode_id)
    items = [i for i in query]
    shuffle(items)
    request.session['item_list'] = items
    print('Items List: ')
    print(items)
    print("\n")

    try:
        first_index = items[0]
    except IndexError:
        return render(request, 'tests/no-items.html')

    # Create a new Test Object
    test = Test(lesson=lesson, category=lesson.category, mode=mode, user=request.user, type=1)
    test.save()
    print(mode)

    return redirect('question-review', first_index, test.id)


class QuestionView(View):

    def dispatch(self, request, *args, **kwargs):
        self.answers = self.get_answers()
        self.inputs = None
        self.question = Item.objects.get(pk=kwargs['item_id'])
        self.status_obj = Status.objects.get(user=request.user)
        self.test = Test.objects.get(id=kwargs['test_id'])
        self.test_mode = self.test.mode.id
        self.test_type = self.test.type

        if self.test_type == 0:
            self.session = self.get_session(self.test)
        else:
            self.session = None

        if self.test_mode == 3:
            self.test_mode = randint(1, 2)

        self.items = self.get_items()
        self.item_tracking = self.get_item_tracking()

        return super(QuestionView, self).dispatch(request, *args, **kwargs)

    def get_session(self, test):
        try:
            session = Session.objects.get(user=self.request.user, lesson=test.lesson)
        except Session.DoesNotExist:
            session = Session(user=self.request.user, lesson=test.lesson)
            session.save()
        return session

    def get_answers(self):
        query = []
        try:
            query = Option.objects.filter(item__id=self.kwargs['item_id']).values_list('name', flat=True)
        except Exception as e:
            print(e)
        answers = [i for i in query]
        shuffle(answers)
        return answers

    def get_item_tracking(self):
        item_tracking, created = ItemTracking.objects.get_or_create(
            item=self.question,
            user=self.request.user,
            lesson=self.test.lesson
        )
        return item_tracking

    def update_tracking(self, answer, item, item_tracking, test):
        question = Question(answer=answer, item=item, test=test)
        print('User Answer: ')
        print(answer)
        print("\n")
        print('Answer: ')
        print(item.option_set.last().name)
        print("\n")
        if answer == item.option_set.last().name:
            # Increase the item score
            question.result = 1
            item_tracking.correct += 1
            result = True
        else:
            # Decrease the item negative score
            question.result = 0
            item_tracking.incorrect += 1
            result = False

        question.save()
        item_tracking.save()
        return result

    def update_box(self, correct):
        if correct:
            # Increase the item score
            if self.item_tracking.box == -1:
                self.item_tracking.box = self.session.index
                self.item_tracking.next = (self.item_tracking.counter + self.session.index) % 10
                self.item_tracking.counter += 1
            elif self.session.index == 9 and self.item_tracking.box == 0:
                self.item_tracking.box = 10
                self.item_tracking.next = -1
                self.item_tracking.counter = 2
            elif self.session.index == self.item_tracking.box - 1:
                self.item_tracking.box = 10
                self.item_tracking.next = -1
                self.item_tracking.counter = 2
            else:
                self.item_tracking.next = (self.item_tracking.counter + self.session.index) % 10
                self.item_tracking.counter += 1
        else:
            # Increase the item negative score
            self.item_tracking.box = -1
            self.item_tracking.counter = 2

        print("Box: ")
        print(str(self.item_tracking.box))
        print("\n")
        print('Next Session: ')
        print(str(self.item_tracking.next))
        print("\n")
        print('Item Counter: ')
        print(str(self.item_tracking.counter))
        print("\n")
        self.item_tracking.save()

    def update_items(self):
        item = None
        try:
            item = self.items.pop(0)

            # Change the index and clear in case needed
            if self.test_type == 0:
                self.status_obj.question_index = self.items[0]
                self.status_obj.save()
        except IndexError:
            print("No more Items")
            if self.test_type == 0:
                self.status_obj.question_index = -1
                self.status_obj.save()

        except Exception as e:
            print("Different Error to index Array")
            print(e)
        return item

    def get_items(self):
        if self.test_type == 0:
            return self.status_obj.items
        else:
            return self.request.session['item_list']

    def calculate_result(self):
        total_answers = Question.objects.filter(test=self.test).count()
        correct_answers = Question.objects.filter(test=self.test, result=1).count()
        print('Correct answers: ')
        print(str(correct_answers))
        print("\n")
        if correct_answers != 0:
            test_score = int((correct_answers / total_answers) * 100)
        else:
            test_score = 0
        print('Test Score: ')
        print(test_score)
        print("\n")
        self.test.score = test_score
        self.test.save()
        # Remove the flag of pending test
        if self.test_type == 0:
            self.status_obj.in_test = False
            self.status_obj.current_test = None
            self.status_obj.save()
            if self.session.index > 8:
                self.session.index = 0
            else:
                self.session.index += 1
                self.session.last_session = datetime.today()
                self.session.save()

    def append_item(self, item):
        if self.test_type == 0:
            self.items.append(item)
            self.status_obj.save()
        else:
            self.items.append(item)

    def post(self, request, *args, **kwargs):
        print("-----Question POST Logs-----")
        answer = request.POST['answer']
        correct = self.update_tracking(answer, self.question, self.item_tracking, self.test)
        # if self.test_type == 0 and not self.repeat:
        if self.test_type == 0:
            self.update_box(correct)
        # After saving the answer correctly then we move the index to nex item in the session
        item = self.update_items()
        if not correct:
            self.append_item(item)
        if not self.items:
            # Calculate and save the score of the test before going to the next view
            self.calculate_result()
            return redirect('test-result', self.test.id)
        else:
            return HttpResponseRedirect('/tests/question/' + str(self.items[0]) +
                                        '/' + str(self.test.id) + '/' + str(self.test_type))

    def get(self, request, *args, **kwargs):
        print("-----Question GET Logs-----")
        print("Items List: ")
        print(self.items)
        print("\n")
        print("Item Options: ")
        print(self.answers)
        print("\n")
        print('Question: ')
        print(self.question.name)
        print("\n")
        try:
            # Shows the current index
            print("Current Index :")
            print(str(self.items[0]))
            print("\n")

        except IndexError:
            print("No more items")

        except Exception as e:
            print(e)

        return render(request, 'tests/question.html', {'inputs': self.answers,
                                                       'question': self.question.name,
                                                       'test_mode': self.test_mode
                                                       })
