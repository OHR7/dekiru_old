# Django
from django.forms import ChoiceField, Form, ModelForm, RadioSelect

# local Django
from items.models import Option
from tests.models import Question


class QuestionForm(ModelForm):

    class Meta:
        model = Question
        fields = ['answer']


class MultipleQuestionForm(Form):

    def __init__(self, *args, **kwargs):
        self.item_id = kwargs.pop('item_id')
        super(MultipleQuestionForm, self).__init__(*args, **kwargs)
        self.fields['answers'].choices = Option.objects.filter(item__id=self.item_id).values_list('name', 'name')

    answers = ChoiceField(widget=RadioSelect())
