# Django
from django.conf.urls import url

# local Django
from .views import create_test, TestMenu, TestResult, TestHistory, create_review, QuestionView

urlpatterns = [
    url(r'^create-review/(?P<lesson_id>[-\w]+)/(?P<mode_id>[-\w]+)/(?P<test_filter>[-\w]+)/', create_review, name='create-review'),
    url(r'^create-test/(?P<lesson_id>[-\w]+)/(?P<mode_id>[-\w]+)/', create_test, name='create-test'),
    # url(r'^question-review/(?P<item_id>[-\w]+)/(?P<test_id>[-\w]+)/', question_review, name='question-review'),
    # url(r'^question-test/(?P<item_id>[-\w]+)/(?P<test_id>[-\w]+)/', question_test, name='question-test'),
    url(r'^test-history/(?P<lesson>[-\w]+)/', TestHistory.as_view(), name='test-history'),
    url(r'^test-menu/(?P<lesson>[-\w]+)/', TestMenu.as_view(), name='test-menu'),
    url(r'^test-result/(?P<pk>[-\w]+)/', TestResult.as_view(), name='test-result'),
    url(r'^question/(?P<item_id>[-\w]+)/(?P<test_id>[-\w]+)/(?P<repeat>[-\w]+)/', QuestionView.as_view(), name='question'),
]
