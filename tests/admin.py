# Django
from django.contrib import admin

# local Django
from .models import Question, Session, Test, TestMode


# Register your models here.


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'answer')


class SessionAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'lesson')


class TestAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'lesson')


class TestModeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


admin.site.register(Question, QuestionAdmin)
admin.site.register(Session, SessionAdmin)
admin.site.register(Test, TestAdmin)
admin.site.register(TestMode, TestModeAdmin)
