# Django
from django.contrib import admin

# local Django
from .models import ClassRoom, School, Student, Status


# Register your models here.


class ClassRoomAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class SchoolRoomAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class StudentRoomAdmin(admin.ModelAdmin):
    list_display = ('id', 'user')


class StatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'current_test')

admin.site.register(ClassRoom, ClassRoomAdmin)
admin.site.register(School, SchoolRoomAdmin)
admin.site.register(Student, StudentRoomAdmin)
admin.site.register(Status, StatusAdmin)
