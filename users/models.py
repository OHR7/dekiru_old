# Django
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models


# Create your models here.


class School(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return '{} {}'.format(self.pk, self.name)


class ClassRoom(models.Model):
    name = models.CharField(max_length=30)
    school = models.ForeignKey(School)

    def __str__(self):
        return '{} {}'.format(self.pk, self.name)


# TODO add perm when user is created
class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}'.format(self.pk, self.user.username)


class Status(models.Model):
    current_test = models.IntegerField(default=None, null=True)
    in_test = models.BooleanField(default=False)
    items = ArrayField(models.IntegerField(), default=[])
    question_index = models.IntegerField(default=None, null=True)
    user = models.ForeignKey(User)
