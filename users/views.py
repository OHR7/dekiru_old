# Django
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render, resolve_url
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

# local Django
from dekiru.decorators import check_test
from users.forms import UserLoginForm
from users.models import Status


# Create your views here.

@method_decorator(check_test, name='dispatch')
class IndexView(TemplateView):
    template_name = "index.html"


@method_decorator(login_required(login_url="/login/"), name='dispatch')
@method_decorator(check_test, name='dispatch')
class HomeView(TemplateView):
    template_name = "home.html"


def user_login(request):
    form = UserLoginForm(request.POST or None)
    print(form)

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                # Redirect to index page.
                return redirect('user-setup')
            else:
                # Return a 'disabled account' error message
                return HttpResponse("You're account is disabled.")
        else:
            # Return an 'invalid login' error message.
            print("invalid login details " + username + " " + password)
            return render(request, 'accounts/login.html', {"form": form})
    else:
        # the login is a  GET request, so just show the user the login form.
        return render(request, 'accounts/login.html', {"form": form})


def user_logout(request):
    logout(request)
    # next_page = resolve_url(self.next_page)
    return redirect('home')


def user_setup(request):
    # Custom code to save the Student in the Session
    status_obj, created = Status.objects.get_or_create(user=request.user)
    status_obj.save()
    print('User log in: ')
    print(request.user)

    try:
        print(str(status_obj.in_test))
        if status_obj.in_test:
            return HttpResponseRedirect(
                '/tests/question/' + str(request.session['index']) +
                '/' + str(request.session['current_test'])
            )
        else:
            # Redirect to index page.
            return HttpResponseRedirect("/home")
    except KeyError:
        # The user is not in test and just log in
        print("no session obj")
        status_obj.in_test = False
        status_obj.save()
        # Redirect to index page.
        return HttpResponseRedirect("/home")
