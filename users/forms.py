from django.contrib.auth import get_user_model
from django import forms
from django.template import Template

User = get_user_model()


class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'input-field'}
    ))
    keep_logged = forms.BooleanField(required=False,
                                     label="Keep me logged in!!!",
                                     widget=forms.CheckboxInput(
                                        attrs={'class': 'input-field'}
                                        )
                                     )

    def clean(self):
        cleaned_data = super(UserLoginForm, self).clean()
        if cleaned_data.get('email') == 'john@doe.com':
            raise forms.ValidationError('John, come on. You are blocked.')

