# Django
from django import forms

# local Django
from items.models import Item, Lesson


class ItemLessonForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.lesson_id = kwargs.pop('lesson_id')
        self.fields['items'].choices = Item.objects.filter(lesson__id=self.lesson_id).values_list('id', 'name')
        super(ItemLessonForm, self).__init__(*args, **kwargs)
    answers = forms.ChoiceField()


class LessonItemForm(forms.ModelForm):

    class Meta:
        model = Lesson
        fields = ['items']
        widgets = {
            'items': forms.CheckboxSelectMultiple()
        }


class LessonForm(forms.ModelForm):

    class Meta:
        model = Lesson
        fields = ['category', 'items', 'name', 'subcategory']
        widgets = {
            'items': forms.CheckboxSelectMultiple()
        }
