# Django
from django.contrib.auth.models import User
from django.db import models

# local Django
from users.models import Student

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return '{} {}'.format(self.pk, self.name)


class SubCategory(models.Model):
    name = models.CharField(max_length=30)
    user = models.ForeignKey(User)

    def __str__(self):
        return '{} {}'.format(self.pk, self.name)


class Type(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return '{} {}'.format(self.pk, self.name)


class Item(models.Model):
    category = models.ForeignKey(Category)
    name = models.CharField(max_length=30)
    subcategory = models.ManyToManyField(SubCategory)
    type = models.ForeignKey(Type)
    user = models.ForeignKey(User, default=1)

    def __str__(self):
        return '{} {}'.format(self.pk, self.name)


class Lesson(models.Model):
    category = models.ForeignKey(Category)
    is_custom = models.BooleanField(default=True)
    items = models.ManyToManyField(Item)
    name = models.CharField(max_length=30)
    subcategory = models.ManyToManyField(SubCategory)
    user = models.ForeignKey(User, default=1)

    def __str__(self):
        return '{} {} {}'.format(self.pk, self.name, self.user)


class ItemTracking(models.Model):
    box = models.IntegerField(default=-1)
    correct = models.IntegerField(default=0)
    counter = models.IntegerField(default=2)
    incorrect = models.IntegerField(default=0)
    item = models.ForeignKey(Item, related_name='itemtracking')
    lesson = models.ForeignKey(Lesson)
    next = models.IntegerField(default=-1)
    user = models.ForeignKey(User)

    def __str__(self):
        return self.item.__str__()

    @property
    def percentage(self):
        try:
            return int((self.incorrect/self.correct) * 100)
        except:
            return 0


class Option(models.Model):
    item = models.ForeignKey(Item)
    name = models.CharField(max_length=30)

    def __str__(self):
        return '{} {}'.format(self.pk, self.name)
