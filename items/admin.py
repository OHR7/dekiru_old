from django.contrib import admin
from .models import Category, Item, ItemTracking, Lesson, Option, SubCategory, Type


# Register your models here.


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class ItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class ItemTrackingRoomAdmin(admin.ModelAdmin):
    list_display = ('id', 'item')


class LessonAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class OptionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class TypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


admin.site.register(Category, CategoryAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(ItemTracking, ItemTrackingRoomAdmin)
admin.site.register(Lesson, LessonAdmin)
admin.site.register(Option, OptionAdmin)
admin.site.register(SubCategory, SubCategoryAdmin)
admin.site.register(Type, TypeAdmin)
