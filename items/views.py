# third-party
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView
from rest_framework import viewsets

# Django
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView, UpdateView

# local Django
from dekiru.decorators import check_test
from items.forms import LessonForm, LessonItemForm
from items.models import Item, ItemTracking, Lesson, SubCategory
from items.serializers import ItemSerializer

# Create your views here.


@method_decorator(login_required(login_url="/login/"), name='dispatch')
@method_decorator(check_test, name='dispatch')
class ItemListViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer


@method_decorator(login_required(login_url="/login/"), name='dispatch')
@method_decorator(check_test, name='dispatch')
class LessonUpdate(UpdateView):
    form_class = LessonItemForm
    model = Lesson
    success_url = '/items/lessons/1/'
    template_name = "items/lesson-modifier.html"




@method_decorator(login_required(login_url="/login/"), name='dispatch')
@method_decorator(check_test, name='dispatch')
class LessonCreate(CreateView):
    form_class = LessonForm
    model = Lesson
    success_url = '/items/lessons/1/'
    template_name = "items/lesson-modifier.html"


@method_decorator(login_required(login_url="/login/"), name='dispatch')
@method_decorator(check_test, name='dispatch')
class LessonListView(ListView):
    model = Lesson
    template_name = "items/lesson-list.html"

    def get_queryset(self):
        if self.kwargs['subcategory'] != '1':
            subcategory = get_object_or_404(SubCategory, pk=self.kwargs['subcategory'])
            return Lesson.objects.filter(Q(user=self.request.user) | Q(user=1), subcategory=subcategory)
        else:
            return Lesson.objects.filter(Q(user=self.request.user) | Q(user=1))


@method_decorator(login_required(login_url="/login/"), name='dispatch')
@method_decorator(check_test, name='dispatch')
class LessonMenuView(DetailView):
    model = Lesson
    template_name = "items/lesson-menu.html"


@method_decorator(login_required(login_url="/login/"), name='dispatch')
@method_decorator(check_test, name='dispatch')
class LessonTracking(DetailView):
    model = Lesson
    template_name = "items/lesson-tracking.html"

    def get_context_data(self, **kwargs):
        context = super(LessonTracking, self).get_context_data(**kwargs)
        item_tracking_list = []
        lesson_pk = self.kwargs.get(self.pk_url_kwarg)
        lesson = Lesson.objects.get(pk=lesson_pk)
        items = lesson.items.filter(itemtracking__isnull=False)

        for item in items:
            item_tracking = ItemTracking.objects.get(item=item.id, user=self.request.user)
            item_tracking_list.append(item_tracking)
            print(item_tracking_list)
            context['item_tracking_list'] = item_tracking_list

        return context
