# Django
from django.conf.urls import url

# local Django
from . import views

urlpatterns = [
    url(r'^lesson-update/(?P<pk>[-\w]+)/', views.LessonUpdate.as_view(), name='lesson-update'),
    url(r'^lesson-create/', views.LessonCreate.as_view(), name='lesson-create'),
    url(r'^lessons/(?P<subcategory>[-\w]+)/', views.LessonListView.as_view(), name='lessons'),
    url(r'^lesson-menu/(?P<pk>[-\w]+)/', views.LessonMenuView.as_view(), name='lesson-menu'),
    url(r'^lesson-tracking/(?P<pk>[-\w]+)/', views.LessonTracking.as_view(), name='lesson-tracking'),
]