"""dekiru URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
# third-party
from rest_framework import routers

# Django
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin

# local Django
from items import views as item_views
from users import views as user_views

router = routers.DefaultRouter()
router.register(r'API/items', item_views.ItemListViewSet)

urlpatterns = [
    # url(r'^', include('django.contrib.auth.urls')),
    url(r'^admin/', admin.site.urls),

    # API URL's
    url(r'^', include(router.urls)),

    # User URL's
    url(r'^login/$', user_views.user_login, name='login'),
    url(r'^logout/$', user_views.user_logout, name='logout'),
    url(r'^user-setup/', user_views.user_setup, name='user-setup'),
    url(r'^dekiru/', user_views.IndexView.as_view(), name='index'),
    url(r'^home/', user_views.HomeView.as_view(), name='home'),

    # Items URL's
    url(r'^items/', include('items.urls')),
    url(r'^tests/', include('tests.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
