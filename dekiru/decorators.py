# Django
from django.http import HttpResponseRedirect

# local Django
from users.models import Status


def check_test(view):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    def decorator(view_func):

        def _wrapped_view(request, *args, **kwargs):
            try:
                status_obj = Status.objects.get(user=request.user)

                if status_obj.in_test:
                    return HttpResponseRedirect('/tests/question-test/' + str(status_obj.question_index) +
                                                '/' + str(status_obj.current_test))
                else:
                    return view_func(request, *args, **kwargs)
            except Status.DoesNotExist:
                return view_func(request, *args, **kwargs)
            except:
                return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator(view)
